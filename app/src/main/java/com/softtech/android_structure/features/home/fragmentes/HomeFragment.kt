package com.softtech.android_structure.features.home.fragmentes

import android.Manifest
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import com.softtech.android_structure.R
import com.softtech.android_structure.base.fragment.BaseFragment
import com.softtech.android_structure.base.utility.GpsStatus
import com.softtech.android_structure.base.utility.GpsStatusListener
import com.softtech.android_structure.base.utility.Utility
import com.softtech.android_structure.base.utility.location.LocationService
import com.softtech.android_structure.domain.entities.Suggest
import com.softtech.android_structure.features.FeatureConstants.SUGGEST_KEY
import com.softtech.android_structure.features.common.CommonState
import com.softtech.android_structure.features.common.showSnackbar
import com.softtech.android_structure.features.home.vm.HomeViewModel
import com.softtech.android_structure.features.map.activities.LocationActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import com.ufelix.utils.Gps
import com.ufelix.utils.service.location.LocationServiceListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeFragment : BaseFragment(){

    private val homeViewModel:HomeViewModel by viewModel()

    private var userLocation:String?=null

    private var locationServiceListener: LocationServiceListener?=null


    override fun layoutResource(): Int =R.layout.fragment_home

    var isTryingLoad=false
    override fun onViewInflated(parentView: View, childView: View) {
        super.onViewInflated(parentView, childView)
        checkPermission()
        locationServiceListener = LocationServiceListener(requireContext(), Intent(requireContext(), LocationService::class.java))

        initObservers()
        initEventHandler()
    }


    private fun checkPermission(){
        val rxPermissions = RxPermissions(this)
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
            .subscribe({ granted ->
                if (!granted) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                        checkPermission()
                    }
                }else{
                    startLocationUpdated()
                }

            },{
                it.printStackTrace()
            })
    }
    private fun initObservers() {
        homeViewModel.apply {
            suggestStateLiveData.observe(this@HomeFragment, Observer {
                handleSuggestState(it)

            })
            locationUpdateLiveData.observe(this@HomeFragment,androidx.lifecycle.Observer {
                userLocation = "${it.latitude},${it.longitude}"
                println("locationUpdateLiveData $userLocation")
                if (isTryingLoad)
                  homeViewModel.getSuggest(userLocation.toString())
            })
        }
        GpsStatusListener(requireContext()).observe(this,androidx.lifecycle.Observer {
            handleGpsChanges(it)
        })
    }




    private fun handleGpsChanges(mode: GpsStatus) {
        println("handleGpsChanges ${mode}")
        when(mode){
            is GpsStatus.Disabled->{
                locationServiceListener!!.unsubscribeFromLocationUpdates()
                Gps.enable(requireContext())
            }
            is GpsStatus.Enabled->{
                startLocationUpdated()
            }
        }
    }


    fun startLocationUpdated() {
        if (!Utility.isMyServiceRunning(LocationService::class.java,requireContext())) {
            println("startLocationUpdated")
            locationServiceListener!!.subscribeToLocationUpdates()
        }
    }

    private fun handleSuggestState(state: CommonState<Suggest>?) {
        Timber.i("handleSuggestState $state")
        when(state){
            is CommonState.LoadingShow->{
                isTryingLoad=false
            }
            is CommonState.Success->{
                navigateToMapActivity(state.data)
                hideAviProgress()
            }
            is CommonState.Error->{
                hideAviProgress()
                showSnackbar(view!!,state.exception.message ?: getString(R.string.some_error))
            }
        }
    }

    private fun showAviProgress() {
        btnSuggest.visibility=View.INVISIBLE
        avloadingIndicatorView.visibility=View.VISIBLE
    }

    private fun hideAviProgress() {
        btnSuggest.visibility=View.VISIBLE
        avloadingIndicatorView.visibility=View.GONE
    }

    private fun initEventHandler() {
        btnSuggest.setOnClickListener {
            Gps.enable(requireContext()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.i(" Gps.enable ${userLocation.toString()}")
                    showAviProgress()
                    if (it && userLocation!=null) {
                        homeViewModel.getSuggest(userLocation.toString())
                    }else{
                        isTryingLoad=true
                    }
                },{it.printStackTrace()})
        }
    }

    override fun onResume() {
        super.onResume()
    }
    fun navigateToMapActivity(suggest: Suggest){
        val intent=Intent(requireContext(),LocationActivity::class.java)
        intent.putExtra(SUGGEST_KEY,suggest)
        startActivity(intent)
        requireActivity().overridePendingTransition(0,0)
    }
}
