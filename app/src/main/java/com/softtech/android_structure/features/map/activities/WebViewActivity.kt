package com.softtech.android_structure.features.map.activities

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.softtech.android_structure.R
import com.softtech.android_structure.base.activity.BaseActivity
import com.softtech.android_structure.features.FeatureConstants
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseActivity() {

    var pageUrl:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        setSupportActionBar(toolbar)
        initWebView()
        pageUrl = getURLFromArgs()
        webView.loadUrl(pageUrl)
    }

    @Throws(IllegalArgumentException::class)
    private fun getURLFromArgs(): String? {
        return  if (intent.extras !=null && intent.extras!!.containsKey(FeatureConstants.WEB_VIEW_URL)){
            intent.extras?.getString(FeatureConstants.WEB_VIEW_URL)
        }else
            throw IllegalArgumentException("${this.javaClass.simpleName} arguments must contains order params")

    }


    private fun initWebView() {
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progress.visibility=View.GONE
            }
        }
    }


    override fun onSupportNavigateUp(): Boolean {
    finish()
    return false
    }

}
