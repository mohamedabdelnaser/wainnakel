package com.softtech.android_structure.features.map.fragmentes

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.softtech.android_structure.R
import com.softtech.android_structure.base.fragment.BaseFragment
import com.softtech.android_structure.base.utility.MapUtility
import com.softtech.android_structure.domain.entities.Suggest
import com.softtech.android_structure.features.FeatureConstants
import com.softtech.android_structure.features.common.CommonState
import com.softtech.android_structure.features.common.showSnackbar
import com.softtech.android_structure.features.home.vm.HomeViewModel
import com.softtech.android_structure.features.map.activities.WebViewActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import com.ufelix.utils.Gps
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_location.*
import timber.log.Timber
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_location.avloadingIndicatorView
import kotlinx.android.synthetic.main.fragment_location.btnSuggest
import org.koin.androidx.viewmodel.ext.android.viewModel


class LocationFragment : BaseFragment(), OnMapReadyCallback {

    companion object {
        const val defaultZoom = 15f
    }



    private val homeViewModel : HomeViewModel by viewModel()

    private var savedInstanceState: Bundle? = null

    private var targetMarker:Marker?=null

    private var map:GoogleMap?=null

    private var suggest:Suggest?=null

    private var latitude:Double?=null

    private var longitude:Double?=null

    private var userLocation:String?=null

    private var isTryingLoad=false

    override fun layoutResource(): Int =R.layout.fragment_location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.savedInstanceState=savedInstanceState
    }
    override fun onViewInflated(parentView: View, childView: View) {
        super.onViewInflated(parentView, childView)
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        initObservers()

        initEventHandler()


    }

    fun onGetSuggest(newSuggest: Suggest?){
        this.suggest=newSuggest
        latitude=suggest?.lat?.let { it }?.toDouble()
        longitude=suggest?.lon?.let { it }?.toDouble()
        targetMarker?.remove()

        if (latitude!=null || longitude !=null)
          targetMarker=  MapUtility.addMarkerAndMoveToSelectedLocation(requireContext(),map,LatLng(latitude!!,longitude!!),R.drawable.ic_pin,defaultZoom,suggest?.name!!)

        initSuggestUI()
        targetMarker?.showInfoWindow()

    }    private fun initSuggestUI() {
        suggest?.apply {
            tvCateName.setText(name)
            tvPrice.setText(this.cat)
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun getArgs():Suggest?{
      return  if (requireActivity().intent.extras !=null && requireActivity().intent.extras!!.containsKey(FeatureConstants.SUGGEST_KEY)){
            requireActivity().intent.extras?.getParcelable<Suggest>(FeatureConstants.SUGGEST_KEY)
        }else
          throw IllegalArgumentException("${this.javaClass.simpleName} arguments must contains order params")

    }

    private fun initEventHandler() {
        ivExpand.setOnClickListener {
            if (loutInfo.isVisible){
                loutInfo.visibility=View.GONE
                ivExpand.setImageResource(R.drawable.ic_arrow_down)

            }else{
                loutInfo.visibility=View.VISIBLE
                ivExpand.setImageResource(R.drawable.ic_up_arrow)

            }
        }

        btnDirection.setOnClickListener {

            suggest?.apply {   MapUtility.goToDirection(requireContext(),latitude,longitude)}
        }
        btnShowImages.setOnClickListener {
            suggest?.link?.let {  navigateToWebView(it) }
        }

        btnSuggest.setOnClickListener {
            Gps.enable(requireContext()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Timber.i(" Gps.enable $it")
                    showAviProgress()
                    if (it && userLocation!=null) {
                        homeViewModel.getSuggest(userLocation.toString())
                    }else{
                        isTryingLoad=true
                    }},{it.printStackTrace()})
        }

        btnUp.setOnClickListener {
            showSnackbar(view!!,getString(R.string.unactive_option))
        }

        btnInfo.setOnClickListener {
          suggest?.apply {    MapUtility.goToGoogleMaps(requireContext(),latitude,longitude) }
        }

        ivFavorite.setOnClickListener {
            showSnackbar(view!!,getString(R.string.unactive_option))
        }
    }

    fun navigateToWebView(pageUrl:String){
        val intent=Intent(requireContext(),WebViewActivity::class.java)
        intent.putExtra(FeatureConstants.WEB_VIEW_URL,pageUrl)
        requireActivity().startActivityFromFragment(this,intent,0)
        requireActivity().overridePendingTransition(0,0)
    }

    private fun initObservers() {
        homeViewModel.apply {
            suggestStateLiveData.observe(this@LocationFragment, Observer {
                handleSuggestState(it)

            })

            locationUpdateLiveData.observe(this@LocationFragment,androidx.lifecycle.Observer {

                userLocation = "${it.latitude},${it.longitude}"
                if (isTryingLoad)
                    homeViewModel.getSuggest(userLocation.toString())
            })

        }
    }

    private fun handleSuggestState(state: CommonState<Suggest>?) {
        Timber.i("handleSuggestState ${state}")
        when(state){
            is CommonState.LoadingShow-> {isTryingLoad=false}
            is CommonState.LoadingFinished->hideAviProgress()
            is CommonState.Success->onGetSuggest(state.data)
            is CommonState.Error-> showSnackbar(view!!,state.exception.message ?: getString(R.string.some_error))
        }
    }


    private fun showAviProgress() {
        btnSuggest.visibility=View.INVISIBLE
        avloadingIndicatorView.visibility=View.VISIBLE
    }

    private fun hideAviProgress() {
        btnSuggest.visibility=View.VISIBLE
        avloadingIndicatorView.visibility=View.GONE
    }

    override fun onMapReady(map: GoogleMap?) {
        println("onMapReady")
        this.map=map
        map!!.mapType = GoogleMap.MAP_TYPE_TERRAIN
        checkPermission()
        map!!.uiSettings.isMyLocationButtonEnabled=true

        onGetSuggest(getArgs())


    }
    private fun checkPermission(){
        val rxPermissions = RxPermissions(this)
            rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe({ granted ->
                    if (!granted) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                            checkPermission()
                        }else{
                            // PopupPermission(requireContext()).show()
                        }
                    }else {
                        map!!.isMyLocationEnabled = true
                    }

                    },{
                        it.printStackTrace()
                    })
    }


    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mapView.onDestroy()

    }


}