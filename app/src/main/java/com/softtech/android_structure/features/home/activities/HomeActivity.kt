package com.softtech.android_structure.features.home.activities

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.softtech.android_structure.R
import com.softtech.android_structure.base.activity.BaseActivity
import com.softtech.android_structure.base.utility.Actions
import com.softtech.android_structure.base.utility.GpsStatus
import com.softtech.android_structure.base.utility.GpsStatusListener
import com.softtech.android_structure.base.utility.Utility
import com.softtech.android_structure.base.utility.location.LocationService
import com.softtech.android_structure.base.utility.receiver.AppReceiver
import com.softtech.android_structure.features.common.CommonState
import com.softtech.android_structure.features.common.hideKeyboard
import com.softtech.android_structure.features.home.vm.HomeViewModel
import com.ufelix.utils.Gps
import com.ufelix.utils.service.location.LocationServiceListener
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.nav_header_home.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class HomeActivity : BaseActivity(){

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navController: NavController

    var receiver: BroadcastReceiver?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        startReceiver()


    }


    override fun onSupportNavigateUp(): Boolean {
        hideKeyboard(this.window)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun startReceiver(){
        receiver = AppReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(Actions.PUSH_LOCATION)
        this.registerReceiver(receiver, intentFilter)

    }
    fun stopReceiver(){
        try {
            this.unregisterReceiver(receiver)
        }catch (e:java.lang.Exception){

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopReceiver()
    }



}
