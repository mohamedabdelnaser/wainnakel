package com.softtech.android_structure.features.home.di

import com.softtech.android_structure.data.repositories.SuggestRepository
import com.softtech.android_structure.data.sources.remote.apis.SuggestAPI
import com.softtech.android_structure.domain.usecases.SuggestUseCase
import com.softtech.android_structure.features.home.vm.HomeViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val homeModule= module {
    factory {  get<Retrofit>().create(SuggestAPI::class.java) }
    factory {  SuggestRepository(get())}
    factory {  SuggestUseCase(get())}
    viewModel { HomeViewModel(get()) }
}