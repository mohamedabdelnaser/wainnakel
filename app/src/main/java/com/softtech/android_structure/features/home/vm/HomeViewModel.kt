package com.softtech.android_structure.features.home.vm

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.softtech.android_structure.base.utility.receiver.LocationBus
import com.softtech.android_structure.domain.entities.Suggest
import com.softtech.android_structure.domain.usecases.SuggestUseCase
import com.softtech.android_structure.features.common.CommonState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel(private val suggestUseCase: SuggestUseCase):ViewModel() {

    private val disposables=CompositeDisposable()
    private val suggestStateMutablelLiveData=MutableLiveData<CommonState<Suggest>>()
    val suggestStateLiveData:LiveData<CommonState<Suggest>> =suggestStateMutablelLiveData

    private val locationUpdateMutablelLiveData=MutableLiveData<Location>()
    val locationUpdateLiveData:LiveData<Location> =locationUpdateMutablelLiveData


    init {
        disposables.add(
            LocationBus.incomingLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    locationUpdateMutablelLiveData.value=(it)
                })
    }
    fun getSuggest(param:String){
        println("params ${param}")
        disposables.add(suggestUseCase.execute(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { suggestStateMutablelLiveData.value=CommonState.LoadingShow  }
                .doFinally {suggestStateMutablelLiveData.value=CommonState.LoadingFinished }
                .subscribe({ suggestStateMutablelLiveData.value=CommonState.Success(it) },
                    {
                    suggestStateMutablelLiveData.value=CommonState.Error(it)
                    it.printStackTrace()
                })
        )

    }

    override fun onCleared() {
        super.onCleared()
        if (!disposables.isDisposed)
            disposables.dispose()
    }
}