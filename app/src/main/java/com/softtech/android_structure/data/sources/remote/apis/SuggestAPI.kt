package com.softtech.android_structure.data.sources.remote.apis

import com.softtech.android_structure.domain.entities.Suggest
import io.reactivex.Single
import retrofit2.http.POST
import retrofit2.http.Query

interface SuggestAPI {
    @POST("GenerateFS.php?")
    fun getSuggest(@Query("uid")  latlng:String): Single<Suggest>


}