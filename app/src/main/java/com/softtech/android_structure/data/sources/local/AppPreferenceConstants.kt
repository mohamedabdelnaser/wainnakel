package com.softtech.android_structure.data.sources.local

object AppPreferenceConstants {

    const val PREFERENCE_FILE_NAME = "app_preferences"
    const val DEFAULT_LOCALE = "default_locale"

}