package com.softtech.android_structure.data.repositories

import com.softtech.android_structure.data.sources.remote.apis.SuggestAPI
import com.softtech.android_structure.domain.entities.Suggest
import io.reactivex.Single

class SuggestRepository (private val suggestAPI: SuggestAPI){
    fun requesGetSuggestAPI(params:String): Single<Suggest> {
        return suggestAPI.getSuggest(params).map { response->response }
    }

}