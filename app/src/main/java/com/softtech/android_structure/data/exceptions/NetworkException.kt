package com.softtech.android_structure.data.exceptions

class NetworkException(override var message: String) : RuntimeException()