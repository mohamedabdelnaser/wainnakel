package com.softtech.android_structure.data.repositories

object RepositoriesConstants {


    const val LANGUAGE_ENGLISH = "en"

    const val LANGUAGE_ARABIC = "ar"

    val SUPPORTED_LANGUAGES = listOf(LANGUAGE_ENGLISH, LANGUAGE_ARABIC)



    const val KEY_PREFRENCE_USER = "USER"

    const val KEY_FIREBASE_TOKEN = "FIREBASE_TOKEN"

    const val KEY_LANGUAGE_CODE: String = "LANGUAGE_CODE"

    const val KEY_DEVICE_INFO: String = "DEVICE_INFO"

}
object LanguageNames{

}