package com.softtech.android_structure.data.sources.remote

import com.softtech.android_structure.application.ApplicationClass


object RemoteConstants {
    const val BASE_URL = "https://wainnakel.com/api/v1/"
    const val CONNECT_TIMEOUT: Long = 60
    const val READ_TIMEOUT: Long = 60
    const val WRITE_TIMEOUT: Long = 60


}