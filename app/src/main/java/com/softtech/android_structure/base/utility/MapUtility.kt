package com.softtech.android_structure.base.utility

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.softtech.android_structure.R








object MapUtility {

     fun addMarkerAndMoveToSelectedLocation(context: Context,googleMap: GoogleMap?, latLng: LatLng,icon:Int=R.drawable.ic_location,zoom: Float,title:String=""):Marker {
        moveCamera(googleMap!!, latLng,zoom)

        return googleMap.addMarker(createMarkerOption(latLng,context,icon).title(title))
    }



    private fun createMarkerOption(
        latLng: LatLng,
        context: Context,
        icon: Int
    ): MarkerOptions {
           val bitmap = getBitmapFromVectorDrawable(context,icon)
        return MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(bitmap))
    }

    private fun getBitmapFromVectorDrawable(context:Context,icon: Int= R.drawable.ic_location): Bitmap {
        val drawable = ContextCompat.getDrawable(context,icon)

        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth,
            drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    fun moveCamera(googleMap: GoogleMap, latLng: LatLng,zoom: Float=15f) {
        googleMap.animateCamera(createCameraUpdate(googleMap, latLng,zoom), 800, null)
    }


     fun createCameraUpdate(googleMap: GoogleMap, latLng: LatLng,zoom:Float=googleMap.cameraPosition.zoom): CameraUpdate {
        return CameraUpdateFactory.newLatLngZoom(latLng, zoom!!)
    }
    fun goToDirection(context: Context, latitude: Double?, longitude: Double?) {

        try {

            val gmm = Uri.parse("http://maps.google.com/maps?daddr=$latitude,$longitude")
            // Uri gmmIntentUri = Uri.parse("google.navigation:q=Taronga+Zoo,+Sydney+Australia");
            val mapIntent = Intent(Intent.ACTION_VIEW, gmm)
            // mapIntent.setPackage("com.google.android.apps.maps");
            context.startActivity(mapIntent)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun goToGoogleMaps(context: Context,latitude: Double?,longitude: Double?){

//        val gmmIntentUri = Uri.parse("geo:$latitude,$longitude")
//        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//        mapIntent.setPackage("com.google.android.apps.maps")
//        context.startActivity(mapIntent)


        val gmmIntentUri = Uri.parse("geo:0,0?q=$latitude,$longitude")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        context.startActivity(mapIntent)
    }
}