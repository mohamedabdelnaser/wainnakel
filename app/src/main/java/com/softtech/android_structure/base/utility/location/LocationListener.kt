package com.ufelix.utils.service.location


interface LocationListener {
    fun subscribeToLocationUpdates()

    fun unsubscribeFromLocationUpdates()
}