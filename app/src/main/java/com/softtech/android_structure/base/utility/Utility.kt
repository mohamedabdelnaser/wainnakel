package com.softtech.android_structure.base.utility

import android.app.ActivityManager
import android.content.Context

object Utility {

    fun isMyServiceRunning(serviceClass: Class<*>, context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                println("isMyServiceRunning?" + true + "")
                return true
            }
        }
        println("isMyServiceRunning?" + false + "")
        return false
    }
}