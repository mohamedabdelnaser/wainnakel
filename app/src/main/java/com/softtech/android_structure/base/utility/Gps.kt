package com.ufelix.utils

import android.content.Context
import com.google.android.gms.location.LocationRequest
import com.softtech.android_structure.base.utility.location.EnableLocationServiceSetting
import io.reactivex.Single

class Gps {

    companion object {
        fun enable(context: Context): Single<Boolean> {
           return EnableLocationServiceSetting.checkLocationServiceSetting(context, getLocationRequest())
        }
        fun getLocationRequest(): LocationRequest {
            val mLocationRequest  = LocationRequest()
            mLocationRequest.interval = 10000
            mLocationRequest.fastestInterval = 5000
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            return mLocationRequest
        }
    }
    sealed class Mode{
        object HighAccurcay:Mode()
        object Enable:Mode()
        object Disabled:Mode()

    }

}