package com.softtech.android_structure.base.activity

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.softtech.android_structure.base.dialogs.CustomeProgressDialog
import com.softtech.android_structure.di.LanguageUseCaseProvider
import timber.log.Timber
import java.util.*

abstract class BaseActivity : AppCompatActivity() {
    lateinit var progressDialog: CustomeProgressDialog

    override fun attachBaseContext(base: Context) {

        println("Langauge local is ${Locale.getDefault()}")
        super.attachBaseContext(LanguageUseCaseProvider.getLanguageUseCase(base).wrap(base))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog= CustomeProgressDialog(this)
    }
    fun showProgressDialog(){
        Timber.i("show dialog ")
        progressDialog.show()
    }

    fun hideProgressDialog(){
        progressDialog.dismiss()
    }


}