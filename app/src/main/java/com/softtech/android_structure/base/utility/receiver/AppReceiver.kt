package com.softtech.android_structure.base.utility.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import com.softtech.android_structure.base.utility.location.LocationService
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


object LocationBus {
    private val bus by lazy { PublishSubject.create<Location>() }
    fun incomingLocation(): Observable<Location> = bus
    fun postLocation(sms: Location) = bus.onNext(sms)
}


class AppReceiver :BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        val location = intent?.getParcelableExtra<Location>(LocationService.LOCATION_KEY)
        println("onReceive $location")

        location?.let { LocationBus.postLocation(it) }

    }
}