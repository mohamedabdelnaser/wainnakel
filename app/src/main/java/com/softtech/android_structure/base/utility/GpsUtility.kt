package com.softtech.android_structure.base.utility
import android.content.Context
import com.google.android.gms.location.LocationRequest
import com.softtech.android_structure.base.utility.location.EnableLocationServiceSetting
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.functions.Function

object GpsUtility {
    fun requsetEnableLocationServiceSetting(context: Context): Single<Boolean> {
        return EnableLocationServiceSetting.checkLocationServiceSetting(context, createLocationRequest())
    }
     fun createLocationRequest() =
        LocationRequest()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setFastestInterval(5000)
            .setInterval(8000)
            .setSmallestDisplacement(5f)




     fun requestLocationServiceSettingFunction(context: Context) =
        Function<Boolean, SingleSource<Boolean>> { granted ->
            if (granted) {
                requsetEnableLocationServiceSetting(context)
            } else {
                Single.error(IllegalArgumentException("Permissions must be granted first"))
            }
        }

sealed class Mode{
    object HighAccurcay:Mode()
    object Enable:Mode()
    object Disabled:Mode()

}

}