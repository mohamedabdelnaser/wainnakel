package com.softtech.android_structure.domain.usecases

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import com.softtech.android_structure.data.repositories.LocaleRepository
import timber.log.Timber
import java.util.*

class LanguageUseCases(private val localeRepository: LocaleRepository) {

    fun changeLanguageTo(langaugeName: String){
        Timber.i("language name ${langaugeName.toString().toLowerCase()}")
        localeRepository.setLanguage(langaugeName)
    }

    fun wrap(context: Context): Context {

        val config = context.resources.configuration

        val language = localeRepository.getLanguage().toLowerCase()

        Timber.i("Langauge local is ${language}")
        val resourc = context.resources

        val dm = resourc.displayMetrics

        if (language != "") {
            val locale = Locale(language)
            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale)
            } else {
                Locale.setDefault(locale)
                config.setLocale(locale)
            }
            resourc.updateConfiguration(config, dm)

        }
        return context.createConfigurationContext(config)
    }

    fun getLanguage()=localeRepository.getLanguage()

   private fun setSystemLocale(config: Configuration, locale: Locale) {
        Locale.setDefault(locale)
        config.setLocale(locale)
    }
}
