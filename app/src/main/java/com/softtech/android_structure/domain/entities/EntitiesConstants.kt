package com.softtech.android_structure.domain.entities

@Suppress("SpellCheckingInspection")
object EntitiesConstants {
    const val EGYPT_COUNTRY_CODE = "EG"
    const val STATUS = "status"
    const val HTTP_STATUS = "httpStatus"

    const val CODE = "code"
    const val MESSAGE = "message"
    const val PAYLOAD = "payload"
    const val ID = "id"
    const val NAME = "name"
    const val ID_NUMBER = "idNumber"
    const val ID_NUMBER_TYPE = "idNumberType"
    const val SOCIAL_STATUS = "socialStatus"
    const val GENDER = "gender"
    const val PHONE_NUMBER = "phoneNumber"
    const val DEVICE_ID = "deviceId"
    const val MONTHLY = "monthly"
    const val HOURLY = "hourly"
    const val NATIONALITIES_ID = "nationalitesId"
    const val IS_SPECIAL="isSpetial"

    const val ICON_NAME = "iconName"
    const val IS_SUPPORTED = "isSupported"
    const val NUMBER = "number"
    const val DISTRICT_ID = "distrectId"
    const val PROMOTION_CODE="promotionCode"
    const val PROMOTION_DISCOUNT="promotionDiscount"

    const val ACCESS_TOKEN = "accessToken"
    const val BRANCH_ID = "branchId"
    const val PLAT_FORM="Android"
}