package com.softtech.android_structure.domain.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Suggest(

	@field:SerializedName("image")
	val image: List<String?>? = null,

	@field:SerializedName("Ulat")
	val ulat: String? = null,

	@field:SerializedName("link")
	val link: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("lon")
	val lon: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("catId")
	val catId: String? = null,

	@field:SerializedName("cat")
	val cat: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("Ulon")
	val ulon: String? = null,

	@field:SerializedName("open")
	val open: String? = null
):Parcelable