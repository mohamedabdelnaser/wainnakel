package com.softtech.android_structure.domain.entities

enum class AppLanguages {
    AR,EN
}