package com.softtech.android_structure.domain.usecases

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.softtech.android_structure.data.repositories.LocationAddressRepository
import com.softtech.android_structure.data.repositories.SuggestRepository
import com.softtech.android_structure.domain.entities.Suggest
import io.reactivex.Single

class SuggestUseCase(private val suggestRepository: SuggestRepository) :
        UseCase<String, Single<Suggest>> {

    override fun execute(param: String?): Single<Suggest> =
            suggestRepository.requesGetSuggestAPI(param!!)

}